#ifndef INPUT_SYSTEM_H
#define INPUT_SYSTEM_H

#include <ape/input/detail/input_detail.h>
#include <ape/input/context_manager.h>

namespace ape {
    namespace input {
        void init();
    }
}

#endif // INPUT_SYSTEM_H
