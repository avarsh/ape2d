#ifndef SCENE_H
#define SCENE_H

#include <ape/core/world.h>
#include <ape/scene/node.h>
#include <ape/graphics/graphics.h>
#include <ape/scene/detail/scene_detail.h>

namespace ape {
    namespace scene {

        void init();
        void render();
    };
}

#endif // SCENE_H
