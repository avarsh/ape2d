#include <ape/graphics/detail/texture_store_detail.h>

namespace ape {
    namespace textureStore {
        namespace detail {
            std::vector<std::shared_ptr<Texture>> textureList;
        }
    }
}
